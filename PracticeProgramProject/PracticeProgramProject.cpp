#include <iostream>
#include <string>

int main()
{
	// Part A: Hello World Haiku
	std::cout << "I can't write haikus, \n";
	std::cout << "I can't think of something good, \n";
	std::cout << "Now I will give up, \n\n";

	// Part B: Variables and Types
	int num1;
	int num2;

	std::cout << "Insert the fisrt number here: ";
	std::cin >> num1;

	std::cout << "Insert the second number here: ";
	std::cin >> num2;

	int num3 = num1 + num2;

	std::cout << "The sum " << num1 << " + " << num2 << " = " << num3 << "\n\n";

	// Part C: Strings
	// Insult Generator
	std::string insult;
	std::cout << "Insert insult here: ";
	std::cin >> insult;

	std::cout << "You are a(n) " << insult << ". Good day sir!" << "\n\n";

	// Part E: Selection
	// Swear Bleeper
	std::string fakeSwear = "clown";
	std::string bleep = "bleep";
	std::string sentance1 = "You are a complete clown!";
	std::string sentance2 = "You are a great friend!";

	if (sentance1.find(fakeSwear) != std::string::npos)
	{
		std::cout << "found!" << '\n';

		sentance1.swap(bleep);
		std::cout << sentance1;
	}
}